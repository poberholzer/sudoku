#!/usr/bin/python

"""
Simple Sudoku generator by (c) Patrick Oberholzer

The main idea of this sudoku generator is that after generating the below
default sudoku field, 2 numbers are swapped with each other 100s of time.
Every time 2 numbers are swapped, it's still a sudoku field by its definition.

1 2 3 4 5 6 7 8 9
4 5 6 7 8 9 1 2 3
7 8 9 1 2 3 4 5 6
2 3 4 5 6 7 8 9 1
5 6 7 8 9 1 2 3 4
8 9 1 2 3 4 5 6 7
3 4 5 6 7 8 9 1 2
6 7 8 9 1 2 3 4 5
9 1 2 3 4 5 6 7 8

Swapping 2 random numbers 100 times:
4 2 8 1 9 7 5 3 6
1 9 7 5 3 6 4 2 8
5 3 6 4 2 8 1 9 7
2 8 1 9 7 5 3 6 4
9 7 5 3 6 4 2 8 1
3 6 4 2 8 1 9 7 5
8 1 9 7 5 3 6 4 2
7 5 3 6 4 2 8 1 9
6 4 2 8 1 9 7 5 3

Randomly removing items per row will make it a solvable sudoku.
. . 8 . 9 . 5 3 6
. 9 7 . 3 . 4 . .
. . 6 . . 8 . 9 7
2 . 1 9 7 5 . . .
. . 5 . 6 4 . 8 1
. . . 2 . . . 7 5
8 1 9 . . 3 . . 2
. 5 3 . . . 8 1 .
. . 2 . . 9 7 . .

"""

import random
import time


def Shift(row, amount):
  """Shifts a list by a defined amount.
  
  Args:
    row: list of row to shift
    amount: integer of amount to shift list
    
  Returns:
    list of shifted list.
  """
  return [(x + amount - 1) % 9 + 1  for x in row]


def Swap(field, x, y):
  """Swaps x and y in a list.

  Args:
    field: 3 dimensional list of sudoku field
    x: integer of x
    y: integer of y

  Returns:
    3 dimensional list of sudku field after swap
  """
  for row in field:
    pos_x = row.index(x)
    pos_y = row.index(y)
    row[pos_x], row[pos_y] = y, x
  return field


def Rand():
  """Generates a random integer between 1 - 9."""
  return int(random.random() * 10) % 9 + 1 


def PrintField(field):
  """Prints Sudoku field."""
  for x in field:
    print ' '.join([str(i) for i in x])


def PrintString(field):
  sudoku = []
  for row in field:
    sudoku.extend(row)
  print ''.join([str(i) for i in sudoku])


def RandomRemove(row):
  """Chooses a random integer and removes this index from the row."""
  row[Rand()-1] = '.'
  return row


def main():
  field = []
  row = range(10)[1:]

  # Generates the main sudoku field.
  for x in range(9):
    if x and not x % 3:
      row = Shift(row, 1)
    field.append(Shift(list(row), 3 * x))

  # Swapping 2 integers in the field 100 times.
  for i in range(100):
    time.sleep(0.01)
    x = Rand()
    time.sleep(0.01)
    y = Rand()
    field = Swap(field, x, y)

  PrintField(field)
  print

  # Generate blanks.
  for i in range(6):
    for row in field:
      row = RandomRemove(row)

  PrintField(field)
  print
  PrintString(field)
      

if __name__ == '__main__':
    main()
